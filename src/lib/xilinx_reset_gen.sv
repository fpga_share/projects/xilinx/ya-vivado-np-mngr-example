////////////////////////////////////////////////////////////////////
// Synchronous reset generator using Xilinx reset synchronizer
////////////////////////////////////////////////////////////////////
// Author: Pavel Yankovets
////////////////////////////////////////////////////////////////////
`default_nettype none
////////////////////////////////////////////////////////////////////
module xilinx_reset_gen
    #(
    parameter TIMEOUT       = 128,
    parameter SYNC_LENGTH   = 4
    )
    (
    input  wire clk,
    output wire reset,
    output wire reset_n
    );
    ////////////////////////////////////////////////////////////////
    // Signals and parameters
    ////////////////////////////////////////////////////////////////
    parameter COUNTER_WIDTH = TIMEOUT>2 ? $clog2(TIMEOUT) : 1;

    (* KEEP = "TRUE" *) reg                         cnt_en      = 1'b1;
    (* KEEP = "TRUE" *) reg  [COUNTER_WIDTH-1:0]    cnt         = {COUNTER_WIDTH{1'b0}};
    
    wire                                            sync_reset_int;
    ////////////////////////////////////////////////////////////////
    // Counter
    ////////////////////////////////////////////////////////////////
    initial begin
        cnt <= {COUNTER_WIDTH{1'b0}};
    end
    always @(posedge clk) begin
        if (cnt < TIMEOUT-1)
            cnt <= cnt + 1'b1;
    end
    ////////////////////////////////////////////////////////////////
    // Counter enable
    ////////////////////////////////////////////////////////////////
    initial begin
        cnt_en <= 1'b1;
    end
    always @(posedge clk) begin
        if (cnt < TIMEOUT-1)
            cnt_en <= 1'b1;
        else
            cnt_en <= 1'b0;
    end
    ////////////////////////////////////////////////////////////////
    // Synchronizing logic
    ////////////////////////////////////////////////////////////////
    xpm_cdc_sync_rst
            #(
            .DEST_SYNC_FF   (SYNC_LENGTH    ),  // integer; range: 2-10
            .INIT           (0              ),  // integer; 0=initialize synchronization registers to 0,
                                                //          1=initialize synchronization registers to 1
            .SIM_ASSERT_CHK (0              )   // integer; 0=disable simulation messages, 1=enable simulation messages
            )
        reset_synchronizer
            (
            .src_rst        (cnt_en         ),
            .dest_clk       (clk            ),
            .dest_rst       (sync_reset_int )
            );
    ////////////////////////////////////////////////////////////////
    // Output resets
    ////////////////////////////////////////////////////////////////
    assign reset            = sync_reset_int;
    assign reset_n          = ~sync_reset_int;
    ////////////////////////////////////////////////////////////////
endmodule
////////////////////////////////////////////////////////////////////
