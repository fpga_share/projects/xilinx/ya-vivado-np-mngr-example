/******************************************************************************
    Модуль:
        top_tb

    Описание:
        Топ-файл testbench

    Комментарии:
        

    Автор:
        Bolshevik
*******************************************************************************/

/* пакеты с кодом для моделирования */
import aux_simulation_pkg::*;
import verbosity_pkg::*;

// synthesis translate_off
`timescale 10ns/1ns
//timeunit        1ns;
//timeprecision   1ns;
// synthesis translate_on

module led_tb;
    /***************************************************************************
     * Sim control
     **************************************************************************/
    parameter CLK_FREQ          = 50.000000;

    logic       reset_n;
    logic       clk;
    real        clk_period;
    int         reset_len = 50; /* tick's */
    int         stop_simulation_delay = 5_000; /* tick's */

    clk_src_bfm
        clock_src
        (
        .clk    (clk        ),
        .reset  (reset_n    )
        );

    /* параметризация и запуск формирователя клока */
    initial
        begin
            clock_src.set_timescale_unit("ns");
            clock_src.set_clockrate(
                CLK_FREQ,
                "MHz"
                );
            clk_period = clock_src.get_clock_period();
            clock_src.clock_start();
        end

    /* параметризация и запуск формирователя сброса */
    initial
        begin
            clock_src.reset_active_state_setup("Lo");
            clock_src.reset_assert();
            #(clk_period * reset_len);
            clock_src.reset_deassert();
        end

    /* test assert */
    initial
        begin
            #(clk_period * 1000);
            assert (reset_n == '0);
        end

    /* отложенная остановка моделирования */
    initial
        begin
            stop_simulation_delayed(clk_period * stop_simulation_delay);
        end

    /***************************************************************************
     * DUT
     **************************************************************************/
     led_top
        led_top_DUT
        (
            .clk    (clk    ),
            .leds   (       )
        );

endmodule
