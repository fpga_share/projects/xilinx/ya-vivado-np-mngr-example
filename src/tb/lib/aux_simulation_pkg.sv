`ifndef _AUX_SIMULATION_PKG_
`define _AUX_SIMULATION_PKG_
`timescale 1ns / 1ps

package aux_simulation_pkg;
    import verbosity_pkg::*;

    class rand_signals;
        rand bit bit_s;
        constraint limit_c { bit_s < 2;}
    endclass 
    
    /***************************************************************************
     * Таск открытия файла.
     * Файлу "file_name" назначается файловый
     * дескриптор "file_descriptor"
     * ---------------------------------------------
     * "r" или "rb"             | Открыть файл для чтения
     * ---------------------------------------------
     * "w" или "wb"             | Создать файл для записи, 
     *                          | или открыть существующий с усечение его до
     *                          | нуля
     * ---------------------------------------------
     * "a" или "ab"             | Создать файл для записи, 
     *                            или открыть с дозаписью в конец файла
     * ---------------------------------------------
     **************************************************************************/
    task automatic open_file(
        input string    file_name,
        input string    file_mode,
        output integer  file_descriptor
        );
        file_descriptor = $fopen(file_name,file_mode);
        if(file_descriptor == 0) begin
            $display({"Error: Failed to open ",file_name});
            $stop;
        end
        $display("Opening file %s", file_name);
    endtask /* open_file */

    /***************************************************************************
     * Таск чтения содержимого файла, на который указывает дескриптор 
     * "file_descriptor". Если таск вызывать внутри тактируемой процедуры 
     * Always, то данные будут вываливаться каждый такт. Тип integer 
     * подразумевает выходную шину типа logic[31:0]. file_data_type - тип 
     * считываемых данных, может принимать значения: %o,%d,%x,%f,%e,%g,%v,%t,%c,
     * %s,%u,%z,%m
     **************************************************************************/
    task automatic read_file(
        input integer           file_descriptor,
        input string            file_data_type,
        output integer          out_data,
        output logic            out_datavalid,
        input logic             out_ready
        );
        
        string                  comment_buff;
        integer                 din_int     = 0;
        integer                 read_status = 0;
        
        out_data         = 0;
        out_datavalid    = 0;

        if (file_descriptor != 0)
            if($fgetc(file_descriptor) == "#") begin
                $fgets(comment_buff,file_descriptor);
                $display("time: %d; in read file comments: %s" ,$stime ,comment_buff);
            end else begin
                if(out_ready) begin
                    read_status     = $fscanf(file_descriptor,file_data_type,din_int);
                end
                if(read_status == 1) begin
                    out_data        = din_int;
                    out_datavalid   = 1;
                end else begin
                    out_data        = 0;
                    out_datavalid   = 0;
                end
            end
    endtask /* read_file */

    /***************************************************************************
     * Таск записи в файл, на который указывает дескриптор "file_descriptor".
     * Если таск вызывать внутри тактируемой процедуры Always, то данные будут 
     * записываться каждый такт, по одному слову. Тип integer подразумевает
     * выходную шину типа logic[31:0]. file_data_type - тип записываемых данных,
     * может принимать значения: %o,%d,%x,%f,%e,%g,%v,%t,%c,%s,%u,%z,%m
     **************************************************************************/
    task automatic write_file(
        input integer           file_descriptor,
        input string            file_data_type,
        input integer           in_data,
        input logic             in_datavalid
        );
        
        string                  text_delimiter = " ";

        if (file_descriptor != 0) begin
            if(in_datavalid) begin
                $fwrite(file_descriptor,{file_data_type,text_delimiter}, in_data);
            end
        end
        $fflush(file_descriptor);
    endtask /* write_file */

    /***************************************************************************
     * Таск записи в файл, на который указывает дескриптор "file_descriptor".
     * Если таск вызывать внутри тактируемой процедуры Always, то данные будут 
     * записываться каждый такт, по одному слову. Тип integer подразумевает
     * выходную шину типа logic[31:0]. file_data_type - тип записываемых данных,
     * может принимать значения: %o,%d,%x,%f,%e,%g,%v,%t,%c,%s,%u,%z,%m
     **************************************************************************/
    task automatic close_file(
        input integer           file_descriptor
        );
        
        $display("Closing file");
        $fclose(file_descriptor);

    endtask /* write_file */

    /***************************************************************************
     * Функция вызывающая останов симуляции через указанное время.
     **************************************************************************/
    task automatic stop_simulation_delayed(
            input integer delay
        );
        #(delay);
        abort_simulation();
    endtask /* abort_simulation_delayed */

endpackage : aux_simulation_pkg

`endif /* _AUX_SIMULATION_PKG_ */