/*******************************************************************************
    Модуль:
                verbosity_pkg
    Описание:
                Пакет инструментов логгирования
    Комментарии:
                Пакет является отредактированным пакетом Altera с целью 
                переиспользовать его в своих проектах. Ибо при просмотре 
                разметки исходного кода его хотелось развидеть.
    Разработчик:
                Altera corp.
*******************************************************************************/

`ifndef _AVALON_VERBOSITY_PKG_
`define _AVALON_VERBOSITY_PKG_

package verbosity_pkg;

    timeunit 1ps;
    timeprecision 1ps;
   
    typedef enum int {
        VERBOSITY_NONE,
        VERBOSITY_FAILURE,
        VERBOSITY_ERROR,
        VERBOSITY_WARNING,
        VERBOSITY_INFO,
        VERBOSITY_DEBUG
    } Verbosity_t;

    Verbosity_t  verbosity = VERBOSITY_INFO;
    string   message = "";
    int      dump_file;
    int      dump = 0;

    /***************************************************************************
     * Функция возвращает текущее значение уровня логгирования
     **************************************************************************/
    function automatic Verbosity_t get_verbosity();
        return verbosity;
    endfunction

    /***************************************************************************
     * Функция устанавливает значение уровня логгирования
     **************************************************************************/
    function automatic void set_verbosity (
        Verbosity_t v
        );
        string       verbosity_str;
        verbosity = v;

        case(verbosity)
            VERBOSITY_NONE:
                verbosity_str = "VERBOSITY_";
            VERBOSITY_FAILURE:
                verbosity_str = "VERBOSITY_FAILURE";
            VERBOSITY_ERROR:
                verbosity_str = "VERBOSITY_ERROR";
            VERBOSITY_WARNING:
                verbosity_str = "VERBOSITY_WARNING";
            VERBOSITY_INFO:
                verbosity_str = "VERBOSITY_INFO";
            VERBOSITY_DEBUG:
                verbosity_str = "VERBOSITY_DEBUG";
            default:
                verbosity_str = "UNKNOWN";
        endcase 

        $sformat(
            message,
            "%m: Setting Verbosity level=%0d (%s)",
            verbosity,
            verbosity_str
            );
        print(VERBOSITY_NONE, message);
    endfunction

    /***************************************************************************
     * Функци вывода сообщений
     *     Если указанный в аргументе уровень важности сообщения меньше или 
     * равен системному уровеню - строка переданная через аргумент будет 
     * выведена в лог.
     *     Если открыт дескриптор файла лога - сообщения в консоли 
     * продублируются в указанный файл.
     **************************************************************************/
    function automatic void print(
        Verbosity_t level,
        string message
        );
        string level_str;

        if (level <= verbosity) begin
            case(level)
                VERBOSITY_NONE:
                    level_str = "";
                VERBOSITY_FAILURE:
                    level_str = "FAILURE:";
                VERBOSITY_ERROR:
                    level_str = "ERROR:";
                VERBOSITY_WARNING:
                    level_str = "WARNING:";
                VERBOSITY_INFO:
                    level_str = "INFO:";
                VERBOSITY_DEBUG:
                    level_str = "DEBUG:";
                default:
                    level_str = "UNKNOWN:";
            endcase 
     
            $display("%t: %s %s",$time, level_str, message);

            if (dump) begin
                $fdisplay(
                    dump_file,
                    "%t: %s %s",
                    $time,
                    level_str,
                    message
                    );
            end
        end
    endfunction

    /***************************************************************************
     * Функция печати строки - разделителя
     **************************************************************************/
    function automatic void print_divider(
        Verbosity_t level
        );
        string message;
        $sformat(
            message, 
            "------------------------------------------------------------"
            );
        print(level, message);
    endfunction
   
    /***************************************************************************
     * Функция открытия файла для записи лога
     **************************************************************************/
    function automatic void open_dump_file (
        string dump_file_name = "avalon_bfm_sim.log"
        );
    
        if (dump) begin
            $sformat(
                message,
                "%m: Dump file already open - ignoring open."
                );
            print(
                VERBOSITY_ERROR,
                message
                );
        end else begin
            dump_file = $fopen(
                dump_file_name,
                "w"
                );
            $fdisplay(
                dump_file,
                "testing dump file"
                );
            $sformat(
                message,
                "%m: Opening dump file: %s",
                dump_file_name
                );
            print(
                VERBOSITY_INFO,
                message
                );
            dump = 1;
        end
    endfunction
 
    /***************************************************************************
     * Функция закрытия файла для записи лога
     **************************************************************************/
    function automatic void close_dump_file();
        if (!dump) begin
            $sformat(
                message,
                "%m: No open dump file - ignoring close."
                );
            print(VERBOSITY_ERROR, message);
        end else begin
            dump = 0;
            $fclose(dump_file);
            $sformat(
                message,
                "%m: Closing dump file"
                );
            print(
                VERBOSITY_INFO,
                message
                );
        end
    endfunction

    /***************************************************************************
     * Функция останова модулирования
     **************************************************************************/
    function automatic void abort_simulation();
        string message;
        $sformat(
            message,
            "%m: Abort the simulation."
            );
        print(
            VERBOSITY_WARNING,
            message
            );
        $stop;
    endfunction
   
endpackage

`endif /* _AVALON_VERBOSITY_PKG_ */

