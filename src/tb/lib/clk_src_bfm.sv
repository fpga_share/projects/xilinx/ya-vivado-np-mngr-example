/*******************************************************************************
    Модуль:
                clk_src_bfm
    Описание:
                Модуль формирования сигналов клока и сброса для модулирования
    Комментарии:
                За основу взяты модули:
                    altera_avalon_clock_source()
                    altera_avalon_reset_source()
    Разработчик:
                /138/Bolshevik
*******************************************************************************/
`timescale 1ns / 1ps
`default_nettype none
import aux_simulation_pkg::*;

module clk_src_bfm (
    output logic clk,
    output logic reset
    );

    int unsigned clk_rate = 10;
    int unsigned clk_unit = 1e6;

// synthesis translate_off
    import verbosity_pkg::*;

    time        half_clk_period;
    shortreal   period_unit             = 1e9;
    string      message                 = "*uninitialized*";
    string      freq_unit               =   (clk_unit == 1)
                                          ? "Hz"
                                          :   (clk_unit == 1e3)
                                            ? "kHz"
                                            : "MHz";
    bit         clk_former_run_state    = 1'b1;

    logic       reset_async             = 0;
    logic       reset_neg               = 0;
    logic       reset_pos               = 0;
    logic       reset_active_state      = 0;
    string      reset_type              = "Pos";
    
    /***************************************************************************
     * Функция вывода в лог сообщения о изменении формируемой частоты
     **************************************************************************/
    function automatic void __mess_freq_change();
        $sformat(
            message,
            "%m: - clk_rate was changed:  %0d %s",
            clk_rate,
            freq_unit
            );
        print(
            VERBOSITY_INFO,
            message
            );
    endfunction

    /***************************************************************************
     * Функция приветствия.
     * Оставлена в целях совместимости с версией Altera
     **************************************************************************/
    function automatic void __hello();
        $sformat(
            message,
            "%m: - Hello from altera_clock_source."
            );
        print(
            VERBOSITY_INFO,
            message
            );
        __mess_freq_change();
        print_divider(VERBOSITY_INFO);
    endfunction

    /***************************************************************************
     * Функция установки единиц измерения периода тактового сигнала
     **************************************************************************/
    function automatic void set_timescale_unit(
        string clock_period_unit = "ns"
        );
        case (clock_period_unit)
            "ps":
                period_unit = 1e12;
            "ns":
                period_unit = 1e9;
            "us" :
                period_unit = 1e6;
            "ms" :
                period_unit = 1e3;
            "s" :
                period_unit = 1;
            default :
                period_unit = 1e9;
        endcase
        $sformat(message, "%m: Period unit: %s", clock_period_unit);
        print(VERBOSITY_INFO, message);
    endfunction

    /***************************************************************************
     * Функция обновления переменной периода тактового сигнала.
     * Если необновить - она вычислится один раз в начале и больше не изменит
     * своего состояния.
     **************************************************************************/
    function automatic void __change_clockrate(
        );
        half_clk_period =   period_unit
                           / (
                               clk_rate
                             * clk_unit
                             * 2
                             );
        
    endfunction

    /***************************************************************************
     * Функция установки частоты
     * Атрументы:
     *     new_clk_rate
     *         - новая частота
     *     clk_unit_string
     *         - строка с размерностью
     **************************************************************************/
    function automatic void set_clockrate(
        time unsigned   new_clk_rate,
        string          clk_unit_string = "MHz"
        );

        clk_rate = new_clk_rate;

        case (clk_unit_string)
            "MHz":
                clk_unit = 1e6;
            "KHz":
                clk_unit = 1e3;
            "Hz" :
                clk_unit = 1;
            default :
                clk_unit = 1e6;
        endcase
        __change_clockrate();
        __mess_freq_change();

    endfunction

    /***************************************************************************
     * Функция вычисления периода тактового сигнала по известным параметрам
     * частоты сигнала и его множетеля (единиц измерения)
     **************************************************************************/
    function automatic shortreal get_clock_period(
        );
        return(period_unit/(clk_rate * clk_unit));
    endfunction

    /***************************************************************************
     * Функция запуска формирователя тактового сигнала
     **************************************************************************/
    task automatic clock_start();
        $sformat(message, "%m: Clock started");
        print(VERBOSITY_INFO, message);
        clk_former_run_state = 1;
    endtask

    /***************************************************************************
     * Функция останова формирователя тактового сигнала
     **************************************************************************/
    task automatic clock_stop();
        $sformat(message, "%m: Clock stopped");
        print(VERBOSITY_INFO, message);
        clk_former_run_state = 0;
    endtask

    /***************************************************************************
     * Функция получения состояния формирователя тактового сигнала
     * Возвращает:
     *     1 - запущен;
     *     0 - остановлен.
     **************************************************************************/
    function automatic get_run_state();
        return clk_former_run_state;
    endfunction

    /***************************************************************************
     * Функция установки сигнала сброса
     **************************************************************************/
    task automatic reset_assert();
        $sformat(message, "%m: reset_async asserted");
        print(VERBOSITY_INFO, message);
     
        if (reset_active_state > 0) begin
            reset_async = 1'b1;
        end else begin
            reset_async = 1'b0;
        end
    endtask

    /***************************************************************************
     * Функция отмены сигнала сброса
     **************************************************************************/
    task automatic reset_deassert();
        $sformat(message, "%m: reset_async deasserted");
        print(VERBOSITY_INFO, message);
       
        if (reset_active_state > 0) begin
            reset_async = 1'b0;
        end else begin
            reset_async = 1'b1;
        end
    endtask

    /***************************************************************************
     * Функция установки активного состояния сигнала сброса
     * Аргументы:
     *     new_reset_active_state - строка со следующими возможными значениями:
     *         - Hi : активный высокий уровень
     *         - Lo : активный низкий уровень
     **************************************************************************/
    function automatic void reset_active_state_setup(
        string  new_reset_active_state
        );
        case (new_reset_active_state)
            "Hi": begin
                reset_active_state = 1;
            end
            "Lo": begin
                reset_active_state = 0;
            end
            default : begin
                reset_active_state = 0;
            end
        endcase
    endfunction

    /***************************************************************************
     * Функция установки типа синзронизации ресета (по фронту, по спаду, 
     * асинхронный).
     * Аргументы:
     *     new_reset_type - строка со следующими возможными значениями:
     *         - "Pos"  : синхронизация по фронту
     *         - "Neg"  : синхронизация по спаду
     *         - "Async": асинхронный
     **************************************************************************/
    function automatic void reset_type_setup(
        string  new_reset_type
        );
        reset_type = new_reset_type;
    endfunction

    /***************************************************************************
     * Инициализация
     **************************************************************************/
    initial begin
        __hello();
        __change_clockrate();
    end

    always 
        begin
            clk = clk_former_run_state;
            #half_clk_period;
            clk = 1'b0;
            #half_clk_period;
        end

    always_ff @(posedge clk) begin : proc_reset_pos_latch
        reset_pos <= reset_async;
    end

    always_ff @(negedge clk) begin : proc_reset_neg_latch
        reset_neg <= reset_async;
    end

    assign reset =   (reset_type == "Pos")
                   ? reset_pos
                   :   (reset_type == "Neg")
                     ? reset_neg
                     : reset_async;

// synthesis translate_on

endmodule

