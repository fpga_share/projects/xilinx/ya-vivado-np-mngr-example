###############################################################################
# IO standarts
###############################################################################
set_property IOSTANDARD LVCMOS33 [get_ports clk]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {leds[0]}]

###############################################################################
# IO places
###############################################################################
set_property PACKAGE_PIN G22 [get_ports clk]
set_property PACKAGE_PIN A23 [get_ports {leds[0]}]
set_property PACKAGE_PIN A24 [get_ports {leds[1]}]
set_property PACKAGE_PIN D23 [get_ports {leds[2]}]
set_property PACKAGE_PIN C24 [get_ports {leds[3]}]
set_property PACKAGE_PIN C26 [get_ports {leds[4]}]
set_property PACKAGE_PIN D24 [get_ports {leds[5]}]
set_property PACKAGE_PIN D25 [get_ports {leds[6]}]
set_property PACKAGE_PIN E25 [get_ports {leds[7]}]

