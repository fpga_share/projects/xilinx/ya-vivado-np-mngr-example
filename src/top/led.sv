/******************************************************************************
    Модуль:
        led_top

    Описание:
        Тестовый модуль

    Комментарии:
        

    Автор:
        Bolshevik
*******************************************************************************/
`default_nettype none
// synthesis translate_off
timeunit        1ns;
// synthesis translate_on

module led_top 
    #(
        LED_WIDTH = 8
    )
    (
    input  wire                     clk,
    output wire [LED_WIDTH-1:0]     leds
    );

localparam CNT_WIDTH = 25;

/******************************************
 * Генератор сигнала сброса
 ******************************************/
(* mark_debug = "true" *) wire rst_n;
xilinx_reset_gen
    reset_gen
        (
            .clk        (clk        ),
            .reset      (           ),
            .reset_n    (rst_n      )
        );

/*****************************************
 Счетчик
 *****************************************/
(* mark_debug = "true" *) logic [CNT_WIDTH-1:0]  cnt;
wire [CNT_WIDTH-1:0] db_cnt;
//test_db
//    test_db_sounter
//    (
//        .clk_50MHz      (clk        ),
//        .Q_0            (db_cnt     )
//    );
always_ff @(posedge clk) begin : counter
    if ( ! rst_n) begin
        cnt <= '0;
    end else begin
        cnt <= cnt + 1;
    end // if
end // always


/*****************************************
 Назначение выходных сигналов
 *****************************************/
assign leds = {LED_WIDTH{db_cnt[CNT_WIDTH-1]}};

endmodule

`default_nettype wire
