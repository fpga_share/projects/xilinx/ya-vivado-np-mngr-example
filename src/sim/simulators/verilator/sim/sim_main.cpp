// DESCRIPTION: Verilator: Verilog example module
//
// This file ONLY is placed under the Creative Commons Public Domain, for
// any use, without warranty, 2017 by Wilson Snyder.
// SPDX-License-Identifier: CC0-1.0
//======================================================================

// For std::unique_ptr
#include <stdio.h>
#include <memory>
#include <string>

#include <getopt.h>

// Include common routines
#include <verilated.h>

// Include model header, generated from Verilating "top.v"
#include "Vtop.h"

// Legacy function required only so linking works on Cygwin and MSVC++
double sc_time_stamp()
{
    return 0;
}

void print_usage(char* prog_name)
{
    printf(
        "Usage: \n"
        "   %s [-l <logdir> | -h] \n"
        "   -l  name of dir for logs\n",
        prog_name);
}

int main(int argc, char** argv)
{
    Verilated::mkdir("log");

    const std::unique_ptr<VerilatedContext> contextp{new VerilatedContext};
    contextp->debug(0);
    contextp->randReset(2);
    contextp->traceEverOn(true);
    contextp->commandArgs(argc, argv);

    extern char* optarg;
    std::string logDir;

    while (1)
    {
        const struct option lopts[] = {
            {"log",  required_argument, 0, 'l'},
            {"help", no_argument,       0, 'h'},
            {NULL,   0,                 0, 0  },
        };

        int c;
        int rc;
        c = getopt_long(argc, argv, "hl:", lopts, NULL);

        if (c == -1)
        {
            break;
        }

        switch (c)
        {
            case 'l':
                logDir = std::string(optarg);
                break;
            case 'h':
                print_usage(argv[0]);
                return (0);
            default:
                print_usage(argv[0]);
                return (-1);
        }
    }

    Verilated::mkdir(logDir.c_str());

    const std::unique_ptr<Vtop> top{
        new Vtop{contextp.get(), "TOP"}
    };

    // Set Vtop's input signals
    top->reset_l    = !0;
    top->clk        = 0;
    top->in_small   = 1;
    top->in_quad    = 0x1234;
    top->in_wide[0] = 0x11111111;
    top->in_wide[1] = 0x22222222;
    top->in_wide[2] = 0x3;

    // Simulate until $finish
    while (!contextp->gotFinish())
    {
        contextp->timeInc(1);  // 1 timeprecision period passes...
        top->clk = !top->clk;

        if (!top->clk)
        {
            if (contextp->time() > 1 && contextp->time() < 10)
            {
                // Assert reset
                top->reset_l = !1;
            }
            else
            {
                // Deassert reset
                top->reset_l = !0;
            }
            // Assign some other inputs
            top->in_quad += 0x12;
        }
        top->eval();

        // Read outputs
        VL_PRINTF(
            "[%" PRId64
            "] "
            "clk=%x "
            "rstl=%x "
            "iquad=%" PRIx64 " -> oquad=%" PRIx64 " owide=%x_%08x_%08x\n",
            contextp->time(),
            top->clk,
            top->reset_l,
            top->in_quad,
            top->out_quad,
            top->out_wide[2],
            top->out_wide[1],
            top->out_wide[0]);
    }

    // Final model cleanup
    top->final();

    // Coverage analysis (calling write only after the test is known to pass)
#if VM_COVERAGE
    std::string covDat(logDir + "/coverage.dat");
    contextp->coveragep()->write(covDat.c_str());
#endif

    return 0;
}
