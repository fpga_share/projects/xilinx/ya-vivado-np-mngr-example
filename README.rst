Example project
###############################################################################
This project is a demo project showing an example of working with the
yet-another-vivado-non-project-manager module.

The project implements a simple counter to control LEDs on a Kintex7 based debug
board.
